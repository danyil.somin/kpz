﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class Form1 : Form
    {
        public class MyEventArgs : EventArgs
        {
            public string s;

            public MyEventArgs(string s)
            {
                this.s = s;
            }
        }

        public delegate void ChangeEventHandler(object sender, EventArgs e);
        public delegate void ChangeEventHandlerMyArgs(object sender, MyEventArgs e);

        event ChangeEventHandler event1;
        event ChangeEventHandlerMyArgs event2;

        public Form1()
        {
            InitializeComponent();

            event1 += (sender, e) =>
            {
                heightUpDown.Value = 1;

                volumeTextBox.Text = ( heightUpDown.Value * widthUpDown.Value * lengthUpDown.Value ).ToString();
            };

            event2 += (sender, e) =>
            {
                volumeTextBox.Text = e.s;
            };
        }

        private void Form1_Load(object sender, EventArgs e) { }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e) { }

        private void button1_Click(object sender, EventArgs e)
        {
            event1(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            event2(sender, new MyEventArgs(""));
        }
    }
}

