﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Globalization;

namespace lab3
{
    class Order
    {
        public int id;
        public string description;
        public float price;

        public Order(int id, string description, float price)
        {
            this.id = id;
            this.description = description;
            this.price = price;
        }
    }

    class Stock
    {
        public int id { get; set; }
        public string adress { get; set; }
    }




    class Customer
    {

        public int id { get; set; }
        public string name { set; get; }
      

        public Customer() { }
        public Customer( int id, string name)
        {
            this.id = id;
            this.name = name;
          
        }

        public override string ToString()
        {
            return String.Format("ID {0}\t Name : {1}", id, name);
        }
    }

    class CustomerComparer<T> : IComparer<T>
        where T : Customer
    {
        public int Compare(T x, T y)
        {
            if (x.id > y.id)
                return 1;
            if (x.id < y.id)
                return -1;
            else return 0;
        }
    }



    class Program
    {
        static void Main(string[] args)
        {


            string[] names = { "Coffe", "Bike", "Clock", "Spoon", "Pollow", "Phone" };
            float[] prices = { 350.0f, 600.0f, 550.0f, 300.0f, 200.0f, 250.0f };

            Order[] MyOrders = new Order[6];
            for (int i = 0; i < 6; i++)
                MyOrders[i] = new Order(i + 1, names[i], prices[i]);

            List<Order> MyList = new List<Order>();
            for (int i = 0; i < 6; i++)
                MyList.Add(new Order(i + 1, names[i], prices[i]));

            Dictionary<int, Order> MyDictionary = new Dictionary<int, Order>();
            for (int i = 0; i < 6; i++)
                MyDictionary.Add(i + 1, new Order(i + 1, names[i], prices[i]));


            var selectH = MyOrders.Where(o => o.id > 3).OrderBy(t => t.description.Length).Reverse();
            Console.WriteLine("\nFrom array:");
            foreach (var i in selectH)
            {
                Console.WriteLine("{0} - {1}" , i.id, i.description);
            }

                IEnumerable<string> selectM = from n in MyList
                                          where n.description.StartsWith("P")
                                          select n.description;
            Console.WriteLine("\nFrom List:");
            foreach (string name in selectM)
            {
                Console.WriteLine("{0}", name);
            }

            IEnumerable<string> selectW = from n in MyDictionary
                                          where n.Value.description.StartsWith("S")
                                          select n.Value.description;

  

            Console.WriteLine("\nFrom Dictionary:");
            foreach (string name in selectW)
            {
                Console.WriteLine("{0}", name);
            }

            Dictionary<int, string> eDictionary =
            MyOrders.ToDictionary(k => k.id,
                                         i => string.Format("{0} {1}",
                                         i.id, i.description));
            Console.WriteLine("\nTo Dictionary:");
            string named = eDictionary[2];
            Console.WriteLine("id 2 - " + named);




            Stock[] MyStocks = new Stock[3];
            MyStocks[0] = new Stock { id = 1, adress = "Shevchenko Street, 28" };
            MyStocks[1] = new Stock { id = 2, adress = "Mazepa Street, 11" };
            MyStocks[2] = new Stock { id = 3, adress = "Bandera Street, 120" };
            Console.WriteLine("\nFrom Stocks:");

            var selectStocks = from n in MyStocks
                          where n.adress.StartsWith("M")
                          select n.adress;
            foreach (var i in selectStocks)
            {
                Console.WriteLine(i);
            }

            var Customer = new[]
            {
                new {Id = 1, Name= "John"},
                new {Id = 2, Name= "Michael"},
                new {Id = 5, Name = "Linda"},
                new {Id = 3, Name = "Anna"},
                new {Id = 4, Name = "Leila"},
                new {Id = 6, Name = "Jack"}

            };

            var selectCus =
                from cus in Customer where cus.Name.StartsWith("L")
                select new { cus.Name };


           
            Console.WriteLine("\nCustomer's names what start with 'L': ");

            foreach (var v in selectCus)
            {
                Console.WriteLine("{0}", v.Name);
            }



            var selectCustomer = Customer.Where(t => t.Id > 3).OrderBy(t => t.Id);

            Console.WriteLine("\nCustomer with Id bigger than 3, ordered by Id: ");

            foreach (var v in selectCustomer)
            {
                Console.WriteLine("{0} - {1}", v.Id, v.Name);
            }

            CustomerComparer<Customer> cp = new CustomerComparer<Customer>();
            List<Customer> dic = new List<Customer>();

            
            dic.Add(new Customer(3, "John"));
            dic.Add(new Customer(2, "Michael"));
            dic.Add(new Customer(4, "Linda"));
            dic.Add(new Customer(5, "Leila"));
            dic.Add(new Customer(1, "Jack"));

            Console.WriteLine("\nSorted by id Customers:");
            dic.Sort(cp);
            foreach (Customer a in dic)
                Console.WriteLine(a);
           

            Order[] MyOrders1 = MyList.ToArray();

            Console.WriteLine("\nList to array:");
            for (int i = 0; i < 6; i++)
                Console.WriteLine("{0} - {1}, price = {2}", MyOrders1[i].id, MyOrders1[i].description, MyOrders1[i].price);
        }
    }
}

