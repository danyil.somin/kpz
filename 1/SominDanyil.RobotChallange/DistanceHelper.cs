﻿using Robot.Common;
using System;

namespace DanyilSomin.RobotChallange
{
    public class DistanceHelper
    {
        public const int collectRange = 2;
        public const int mapWidth = 100;

        public static int DistanceBetween(Position position1, Position position2)
        {
            int dX = Math.Min(Math.Abs(position1.X - position2.X), Math.Abs(Math.Abs(position1.X - position2.X) - mapWidth));
            int dY = Math.Min(Math.Abs(position1.Y - position2.Y), Math.Abs(Math.Abs(position1.Y - position2.Y) - mapWidth));
            return dX + dY;
        }

        public static int DistanceBetweenWarpless(Position position1, Position position2)
        {
            int dX = Math.Abs(position1.X - position2.X);
            int dY = Math.Abs(position1.Y - position2.Y);
            return dX + dY;
        }

        public static bool isCanGetNow(Robot.Common.Robot robot, Position to)
        {
            int dX = Math.Min(Math.Abs(to.X - robot.Position.X), Math.Abs(to.X - robot.Position.X - mapWidth));
            int dY = Math.Min(Math.Abs(to.Y - robot.Position.Y), Math.Abs(to.Y - robot.Position.Y - mapWidth));

            return robot.Energy >= dX * dX + dY * dY;
        }

        public static Position getPositionOnTheWay(Position from, Position to, int maxDelta)
        {
            int dX;
            int dY;

            if (Math.Abs(to.X - from.X) < Math.Abs(to.X - from.X - mapWidth))
            {
                dX = Math.Sign(to.X - from.X) * Math.Min(Math.Abs(to.X - from.X), maxDelta);
            }
            else
            {
                dX = Math.Sign(from.X - to.X) * Math.Min(Math.Abs(to.X - from.X), maxDelta);
            }

            if (Math.Abs(to.Y - from.Y) < Math.Abs(to.Y - from.Y - mapWidth))
            {
                dY = Math.Sign(to.Y - from.Y) * Math.Min(Math.Abs(to.Y - from.Y), maxDelta);
            }
            else
            {
                dY = Math.Sign(from.Y - to.Y) * Math.Min(Math.Abs(to.Y - from.Y), maxDelta);
            }

            return new Position((from.X + dX + mapWidth) % mapWidth, (from.Y + dY + mapWidth) % mapWidth);
        }
    }
}