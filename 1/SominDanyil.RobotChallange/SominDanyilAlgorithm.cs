﻿using Robot.Common;
using System;
using System.Collections.Generic;

namespace DanyilSomin.RobotChallange
{

    public class DanyilSominRobotChallange : IRobotAlgorithm
    {
        public DanyilSominRobotChallange()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            round++;
        }

        public static int robotEnergyToMakeNew = 300;

        public static int round = 0;

        public static Position fingTargetCell(Robot.Common.Robot curRobot, IList<Robot.Common.EnergyStation> stations, IList<Robot.Common.Robot> allRobots)
        {
            EnergyStation energyStation = null;
            int num = int.MaxValue;
            using (IEnumerator<EnergyStation> enumerator = stations.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    EnergyStation current = enumerator.Current;
                    if (isStationFree(current, curRobot, allRobots)) 
                    {
                        int distance = DistanceHelper.DistanceBetween(current.Position, curRobot.Position);
                        if (distance < num)
                        {
                            num = distance;
                            energyStation = current;
                        }
                    }
                }
            }

            Position target = null;
            int curStationsCount = 0;
            int curDist = 100;
            if (energyStation != null)
            {
                for (int i = -2; i <= 2; ++i)
                    for (int j = -2; j <= 2; ++j)
                    {
                        Position curPos = new Position(energyStation.Position.X + i, energyStation.Position.Y + j);
                        if (curPos.X >= 0 && curPos.X < 100
                            && curPos.Y >= 0 && curPos.Y < 100)
                        {
                            foreach(var robot in allRobots)
                            {
                                if (curPos == robot.Position
                                    && robot.Position != curRobot.Position)
                                {
                                    continue;
                                }

                                if (findNearStations(curPos, stations).Count >= curStationsCount
                                    && DistanceHelper.DistanceBetweenWarpless(curRobot.Position, curPos) < curDist)
                                {
                                    target = new Position(curPos.X, curPos.Y);
                                    curStationsCount = findNearStations(curPos, stations).Count;
                                    curDist = DistanceHelper.DistanceBetweenWarpless(curRobot.Position, curPos);
                                }
                            }
                        }
                    }
            }

            return DistanceHelper.DistanceBetween(target, curRobot.Position) < (98 * 2 - round * 4) ? target : null;
        }

        public static IList<Robot.Common.EnergyStation> findNearStations(Position p, IList<Robot.Common.EnergyStation> stations)
        {
            List<Robot.Common.EnergyStation> returnStations = new List<Robot.Common.EnergyStation>();

            foreach (var station in stations)
            {
                if (Math.Abs(station.Position.X - p.X) < 3
                    && Math.Abs(station.Position.Y - p.Y) < 3)
                {
                    returnStations.Add(station);
                }
            }

            return returnStations;
        }

        public static bool isStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            bool onlyFriend = false;

            foreach (var robot in robots)
            {
                if (Math.Abs(robot.Position.X - station.Position.X) < 3
                    && Math.Abs(robot.Position.Y - station.Position.Y) < 3
                    && (!onlyFriend || robot.OwnerName == "Danyil Somin"))
                {
                    if (robot.Position == movingRobot.Position)
                    {
                        onlyFriend = true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static int getStepDelta(int energy)
        {
            if (energy < 20) return 1;
            if (energy < 80) return 2;
            if (round > 35) return 2;
            if (energy < 180) return 3;
            if (energy < 320) return 4;
            if (energy < 500) return 5;
            if (energy < 720) return 6;
            if (energy < 980) return 7;
            return 8;
        }

        public static bool isAbleToDivide(Robot.Common.Robot robot, int robotsAmount, int stationsAmount)
        {
            return robot.Energy > robotEnergyToMakeNew && ((robotsAmount < stationsAmount) || robot.Energy > 1000) && round < 37;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var freeStation = fingTargetCell(robots[robotToMoveIndex], map.Stations, robots);

            if (isAbleToDivide(robots[robotToMoveIndex], robots.Count, map.Stations.Count))
            {
                var cmd = new CreateNewRobotCommand();
                cmd.NewRobotEnergy = robotEnergyToMakeNew - 200;
                return cmd;
            }
            
            if (freeStation == null)
                return null;

            if (freeStation == robots[robotToMoveIndex].Position)
            {
                return new CollectEnergyCommand();
            }

            Position movePoint;
            
            if (DistanceHelper.isCanGetNow(robots[robotToMoveIndex], freeStation))
            {
                movePoint = freeStation;
            }
            else
            {
                movePoint = DistanceHelper.getPositionOnTheWay(
                robots[robotToMoveIndex].Position,
                freeStation,
                getStepDelta(robots[robotToMoveIndex].Energy)
                );
            }

            MoveCommand moveCommand = new MoveCommand();
            moveCommand.NewPosition = movePoint;
            return moveCommand;
        }

        public string Author => "Danyil Somin";

        public string Description => "Danyil Somin lab 1";
    }
}
