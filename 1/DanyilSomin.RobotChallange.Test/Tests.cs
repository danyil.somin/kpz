using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace DanyilSomin.RobotChallange.Test
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestDistance()
        {
            Assert.AreEqual(4, DistanceHelper.DistanceBetween(new Position(1, 1), new Position(2, 4)));
        }
        
        [TestMethod]
        public void TestDistanceWall()
        {
            Assert.AreEqual(4, DistanceHelper.DistanceBetween(new Position(1, 1), new Position(99, 99)));
        }

        [TestMethod]
        public void TestGetPositionOnTheWay()
        {
            int delta = 1;

            Position from = new Position(1, 1);
            Position to = new Position(99, 99);

            Position test = DistanceHelper.getPositionOnTheWay(from, to, delta);

            Assert.AreEqual(0, test.X);
            Assert.AreEqual(0, test.Y);
        }

        [TestMethod]
        public void TestOptimalPositionCalculation()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 1000;
            robot.Position = new Position(7, 7);

            Robot.Common.EnergyStation station1 = new Robot.Common.EnergyStation();
            station1.Position = new Position(1, 1);

            Robot.Common.EnergyStation station2 = new Robot.Common.EnergyStation();
            station2.Position = new Position(5, 5);

            Position test = DanyilSominRobotChallange.fingTargetCell(
                robot,
                new List<Robot.Common.EnergyStation> { station1, station2 },
                new List<Robot.Common.Robot> { robot }
                );

            Assert.AreEqual(3, test.X);
            Assert.AreEqual(3, test.Y);
        }

        [TestMethod]
        public void TestFriendlyRobotStationsIgnore()
        {
            Robot.Common.Robot robot1 = new Robot.Common.Robot();
            robot1.Energy = 1000;
            robot1.Position = new Position(3, 3);

            Robot.Common.Robot robot2 = new Robot.Common.Robot();
            robot2.Energy = 1000;
            robot2.Position = new Position(4, 4);

            Robot.Common.EnergyStation station1 = new Robot.Common.EnergyStation();
            station1.Position = new Position(1, 1);

            Robot.Common.EnergyStation station2 = new Robot.Common.EnergyStation();
            station2.Position = new Position(5, 5);

            Robot.Common.EnergyStation station3 = new Robot.Common.EnergyStation();
            station3.Position = new Position(4, 12);

            Position test = DanyilSominRobotChallange.fingTargetCell(
                robot2,
                new List<Robot.Common.EnergyStation> { station1, station2, station3 },
                new List<Robot.Common.Robot> { robot1, robot2 }
                );

            Assert.AreEqual(4, test.X);
            Assert.AreEqual(10, test.Y);
        }

        [TestMethod]
        public void TestRegularMovement()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 299;
            robot.Position = new Position(1, 1);

            Position target = new Position(12, 12);

            Position test;

            if (DistanceHelper.isCanGetNow(robot, target))
            {
                test = target;
            }
            else
            {
                test = DistanceHelper.getPositionOnTheWay(
                robot.Position,
                target,
                DanyilSominRobotChallange.getStepDelta(robot.Energy)
                );
            }

            Assert.AreEqual(12, test.X);
            Assert.AreEqual(12, test.Y);
        }

        [TestMethod]
        public void TestLimitedEnergyMovement()
        {
            DanyilSominRobotChallange.round = 0;

            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 250;
            robot.Position = new Position(1, 1);

            Position target = new Position(50, 50);

            Position test;

            if (DistanceHelper.isCanGetNow(robot, target))
            {
                test = target;
            }
            else
            {
                test = DistanceHelper.getPositionOnTheWay(
                robot.Position,
                target,
                DanyilSominRobotChallange.getStepDelta(robot.Energy)
                );
            }

            Assert.AreEqual(5, test.X);
            Assert.AreEqual(5, test.Y);
        }

        [TestMethod]
        public void TestEndgameMovement()
        {
            DanyilSominRobotChallange.round = 45;

            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 250;
            robot.Position = new Position(1, 1);

            Position target = new Position(50, 50);

            Position test;

            if (DistanceHelper.isCanGetNow(robot, target))
            {
                test = target;
            }
            else
            {
                test = DistanceHelper.getPositionOnTheWay(
                robot.Position,
                target,
                DanyilSominRobotChallange.getStepDelta(robot.Energy)
                );
            }

            Assert.AreEqual(3, test.X);
            Assert.AreEqual(3, test.Y);
        }

        [TestMethod]
        public void testDuplicationRegular()
        {
            DanyilSominRobotChallange.round = 0;

            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 500;

            Assert.IsTrue(DanyilSominRobotChallange.isAbleToDivide(robot, 40, 120));
        }

        [TestMethod]
        public void testDuplicationEndGame()
        {
            DanyilSominRobotChallange.round = 45;

            Robot.Common.Robot robot = new Robot.Common.Robot();
            robot.Energy = 5000;

            Assert.IsTrue(DanyilSominRobotChallange.isAbleToDivide(robot, 130, 120));
        }
    }
}
